package org.diframework.registry;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

public class BeanContainerTest {

    @Test
    @DisplayName("Test bean container")
    public void testBeanContainerSingleton(){
        BeanContainer b1 = BeanContainer.getInstance();
        BeanContainer b2 = BeanContainer.getInstance();
        Assert.assertNotNull(b1);
        Assert.assertNotNull(b2);
        Assert.assertSame(b1, b2);
    }

    @Test
    @DisplayName("Test Register a bean")
    public void testBeanContainerRegister(){
        BeanContainer b1 = BeanContainer.getInstance();
        b1.register("Hello");
        String s1 = b1.getBean(String.class);
        Assert.assertNotNull(s1);
        Assert.assertThat(s1, CoreMatchers.instanceOf(String.class));
        Assert.assertEquals(s1, "Hello");
    }

    @Test
    @DisplayName("Test Register a bean multiple times")
    public void testBeanContainerRegisterMultipleTimes(){
        BeanContainer b1 = BeanContainer.getInstance();
        b1.register("Hello");
        b1.register("Hello");
        String s1 = b1.getBean(String.class);
        String s2 = b1.getBean(String.class);
        Assert.assertNotNull(s1);
        Assert.assertNotNull(s2);
        Assert.assertEquals(s1, s2);
    }
}