package org.diframework.support;

import org.diframework.annotation.MyBean;
import org.diframework.annotation.Scope;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

public class BeanCreatorFactoryTest {

    @Test
    @DisplayName("Test Bean Creator for Singleton type")
    public void testGetBeanCreatorForSingleton(){
        Assert.assertThat(BeanCreatorFactory.getBeanCreator(SingleBean.class), CoreMatchers.instanceOf(SingletonBeanCreator.class)); ;
    }

    @Test
    @DisplayName("Test Bean Creator for Prototype type")
    public void testGetBeanCreatorForPrototype(){
        Assert.assertThat(BeanCreatorFactory.getBeanCreator(PrototypeBean.class), CoreMatchers.instanceOf(ProtoTypeBeanCreator.class)); ;
    }
}

@MyBean(scope = Scope.SINGLETON)
class SingleBean{

}

@MyBean(scope = Scope.PROTOTYPE)
class PrototypeBean{

}

