package org.diframework.support;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

public class SingletonBeanCreatorTest {

    @Test
    @DisplayName("Test singleton bean creation")
    public void testCreateSingletonBean() throws Exception{
        BeanCreator beanCreator = new SingletonBeanCreator();
        Object o1 = beanCreator.createInstance(SingleBean.class);
        Object o2 = beanCreator.createInstance(SingleBean.class);
        Assert.assertNotNull(o1);
        Assert.assertNotNull(o2);
        Assert.assertSame(o1, o2);
    }
}
