package org.diframework.support;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

public class ProtoTypeBeanCreatorTest {

    @Test
    @DisplayName("Test prototype bean creation")
    public void testCreatePrototypeBean() throws Exception{
        BeanCreator beanCreator = new ProtoTypeBeanCreator();
        Object o1 = beanCreator.createInstance(PrototypeBean.class);
        Object o2 = beanCreator.createInstance(PrototypeBean.class);
        Assert.assertNotNull(o1);
        Assert.assertNotNull(o2);
        Assert.assertNotSame(o1, o2);
    }
}
