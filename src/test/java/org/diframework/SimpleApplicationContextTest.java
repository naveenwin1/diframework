package org.diframework;


import org.diframework.exception.CircularDependencyException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

public class SimpleApplicationContextTest {

    private ApplicationContext targetObject = new SimpleApplicationContext();

    @Test
    @DisplayName("Test get Single Bean")
    public void testGetSingleBean() throws Exception {
        SingleBean t  = targetObject.getBean(SingleBean.class);
        Assert.assertNotNull(t);
    }

    @Test
    @DisplayName("Test get Single associated Bean")
    public void testGetDependentBean() throws Exception {
        SingleAssociatedBean t  = targetObject.getBean(SingleAssociatedBean.class);
        Assert.assertNotNull(t);
        Assert.assertNotNull(t.getDependentBean1());
    }

    @Test
    @DisplayName("Test Singleton bean property")
    public void testSingletonBean() throws Exception {
        SingleBean t1  = targetObject.getBean(SingleBean.class);
        SingleBean t2  = targetObject.getBean(SingleBean.class);
        Assert.assertNotNull(t1);
        Assert.assertNotNull(t2);
        Assert.assertSame(t1, t2);
    }

    @Test
    @DisplayName("Test Singleton bean with multiple associations")
    public void testSingletonBeanWithMultipleAssociations() throws Exception {
        MultipleAssociatedBean t1  = targetObject.getBean(MultipleAssociatedBean.class);
        Assert.assertNotNull(t1);
        Assert.assertNotNull(t1.getDependentBean1());
        Assert.assertNotNull(t1.getDependentBean2());
    }

    @Test
    @DisplayName("Test Prototype bean with associated prototype bean")
    public void testPrototypeBean() throws Exception {
        AssociatedProtoTypeBean t1  = targetObject.getBean(AssociatedProtoTypeBean.class);
        AssociatedProtoTypeBean t2  = targetObject.getBean(AssociatedProtoTypeBean.class);
        Assert.assertNotNull(t1);
        Assert.assertNotNull(t2);
        Assert.assertNotSame(t1, t2);
    }

    @Test
    @DisplayName("Test Prototype bean with associated prototype bean")
    public void testPrototypeBeanWithAssociation() throws Exception {
        AssociatedProtoTypeBean t1  = targetObject.getBean(AssociatedProtoTypeBean.class);
        AssociatedProtoTypeBean t2  = targetObject.getBean(AssociatedProtoTypeBean.class);
        Assert.assertNotNull(t1);
        Assert.assertNotNull(t2);
        Assert.assertNotSame(t1, t2);
        Assert.assertNotSame(t1.getDependentProtoTypeBean(), t2.getDependentProtoTypeBean());
    }

    @Test
    @DisplayName("Test Prototype bean with associated singleton bean")
    public void testPrototypeBeanWithSingletonAssociation() throws Exception {
        ProtoTypeBeanWithSingleton t1  = targetObject.getBean(ProtoTypeBeanWithSingleton.class);
        ProtoTypeBeanWithSingleton t2  = targetObject.getBean(ProtoTypeBeanWithSingleton.class);
        Assert.assertNotNull(t1);
        Assert.assertNotNull(t2);
        Assert.assertNotSame(t1, t2);
        Assert.assertSame(t1.getDependentBean1(), t2.getDependentBean1());
    }


    @Test
    @DisplayName("Test Not registered as bean")
    public void testGetBeanForNotABean() throws Exception {
        NotABean t1  = targetObject.getBean(NotABean.class);
        Assert.assertNull(t1);
    }

    @Test
    @DisplayName("Test Circular dependency bean")
    public void testGetBeanWithCircularDependency() throws Exception {
        CircularDependencyException thrown = Assertions.assertThrows(
                CircularDependencyException.class,
                () -> targetObject.getBean(CircularBean1.class),
                "Expected getBean() to throw, but it didn't"
        );
        Assert.assertNotNull(thrown);
    }

}
