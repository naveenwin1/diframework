package org.diframework;

import lombok.Getter;
import org.diframework.annotation.MyAutoWire;
import org.diframework.annotation.MyBean;
import org.diframework.annotation.Scope;

public class TestResource {

}

@MyBean
class SingleBean{

}

@MyBean
@Getter
class SingleAssociatedBean{
    @MyAutoWire
    private DependentBean1 dependentBean1;
}

@MyBean
@Getter
class MultipleAssociatedBean{
    @MyAutoWire
    private DependentBean1 dependentBean1;
    @MyAutoWire
    private DependentBean2 dependentBean2;
}

@MyBean
class DependentBean1{

}

@MyBean
class DependentBean2{

}

@MyBean(scope = Scope.PROTOTYPE)
class SingleProtoTypeBean {

}

@MyBean(scope = Scope.PROTOTYPE)
@Getter
class AssociatedProtoTypeBean {
    @MyAutoWire
    private DependentProtoTypeBean dependentProtoTypeBean;
}


@MyBean(scope = Scope.PROTOTYPE)
class DependentProtoTypeBean {

}

@MyBean(scope = Scope.PROTOTYPE)
@Getter
class ProtoTypeBeanWithSingleton {
    @MyAutoWire
    private DependentBean1 dependentBean1;
}


@MyBean
@Getter
class CircularBean1{
    @MyAutoWire
    CircularBean2 circularBean2;
}

@MyBean
@Getter
class CircularBean2{
    @MyAutoWire
    CircularBean1 circularBean1;
}

class NotABean {

}

