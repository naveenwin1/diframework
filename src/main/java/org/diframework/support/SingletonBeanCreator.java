package org.diframework.support;

import org.diframework.registry.BeanContainer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class SingletonBeanCreator implements BeanCreator {
    private final BeanContainer beanContainer = BeanContainer.getInstance();

    public <T> T createInstance(Class<T> classVar) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        if(beanContainer.contains(classVar)){
            return beanContainer.getBean(classVar);
        }
        Constructor<T> constructor = classVar.getDeclaredConstructor();
        constructor.setAccessible(true);
        T t = constructor.newInstance();
        beanContainer.register(t);
        return t;
    }
}
