package org.diframework.support;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ProtoTypeBeanCreator implements BeanCreator {

    @Override
    public <T> T createInstance(Class<T> classVar) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<T> constructor = classVar.getDeclaredConstructor();
        constructor.setAccessible(true);
        return constructor.newInstance();
    }
}
