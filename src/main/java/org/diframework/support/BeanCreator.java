package org.diframework.support;

import java.lang.reflect.InvocationTargetException;

public interface BeanCreator {

    <T> T createInstance(Class<T> classVar) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;
}
