package org.diframework.support;

import org.diframework.annotation.MyBean;
import org.diframework.annotation.Scope;

import java.util.HashMap;
import java.util.Map;

public class BeanCreatorFactory {

    private static final Map<Scope, BeanCreator> BEAN_CREATOR_POOL = new HashMap<>();

    static {
        BEAN_CREATOR_POOL.put(Scope.SINGLETON, new SingletonBeanCreator());
        BEAN_CREATOR_POOL.put(Scope.PROTOTYPE, new ProtoTypeBeanCreator());
    }

    public static <T> BeanCreator getBeanCreator(Class<T> classVar){
        if(classVar.isAnnotationPresent(MyBean.class)){
            MyBean myBean = classVar.getAnnotation(MyBean.class);
            return  BEAN_CREATOR_POOL.get(myBean.scope());
        }
        return null;
    }
}
