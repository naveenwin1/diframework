package org.diframework.exception;

public class CircularDependencyException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Circular dependency detected for beans while initialization";
    }
}
