package org.diframework.registry;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BeanContainer {

    private static final Map<Class<?>, Object> objectContainer = new ConcurrentHashMap<>();
    private static final BeanContainer beanContainer = new BeanContainer();

    private BeanContainer(){}

    public <T> T getBean(Class<T> c){
        return (T) objectContainer.get(c);
    }

    public <T> void register(T t) {
        objectContainer.put(t.getClass(), t);
    }

    public <T> boolean contains(Class<T> c){
        return objectContainer.containsKey(c);
    }

    public static BeanContainer getInstance(){
        return beanContainer;
    }
}
