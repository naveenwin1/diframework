package org.diframework.annotation;

public enum Scope {
    PROTOTYPE,
    SINGLETON
}
