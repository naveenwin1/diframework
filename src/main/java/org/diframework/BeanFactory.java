package org.diframework;

public interface BeanFactory {

    <T> T getBean(Class<T> var1) throws Exception;
}
