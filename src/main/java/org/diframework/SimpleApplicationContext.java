package org.diframework;

import org.diframework.annotation.MyAutoWire;
import org.diframework.exception.CircularDependencyException;
import org.diframework.support.BeanCreator;
import org.diframework.support.BeanCreatorFactory;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public class SimpleApplicationContext extends ApplicationContext{
    @Override
    public <T> T getBean(Class<T> classVar) throws Exception {
        return getBeanHelper(classVar, new HashSet<>());
    }

    private <T> T getBeanHelper(Class<T> classVar, Set<Class<?>> visitedNodes) throws Exception {
        visitedNodes.add(classVar);
        BeanCreator creator = BeanCreatorFactory.getBeanCreator(classVar);
        if (creator != null) {
            T currentObject = creator.createInstance(classVar);
            Field[] fields = classVar.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(MyAutoWire.class)) {
                    field.setAccessible(true);
                    if(visitedNodes.contains(field.getType())){
                        throw new CircularDependencyException();
                    }
                    field.set(currentObject, getBeanHelper(field.getType(), visitedNodes));
                }
            }
            return currentObject;
        }
        return null;
    }
}
