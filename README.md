# README #



### What is this repository for? ###

#### Quick summary
    
   <p> 
       The dependency injection framework project is a library which is a simple and very basic implementation of DI feature. This has features like getting the bean by their type.
       This library also support scopes of bean like `Prototype` and `Singleton`. The library injects all the dependencies in the dependency tree of the bean requested. This library also detects the cases of circular dependencies. 
   </p>
   
* Version
    * 1.0
    
## Design 

### Annotations
There are 2 annotation in this library `MyBean` which can accept scope parameter and another annotation is `MyAutoWire` which will specify that the bean should be created by framework.

### BeanContainer    
This the class the which will hold the bean by bean type which hides and decouple the bean storage features. There can be multiple Bean Container so that when different scopes to be implemented then we can store and fetch the bean by different score ids

### BeanCreator
These are the classes which will help to create different types of beans like singleton and prototype, each one can different their own way so that the caller can choose based on the input on the requested bean. This is done using a factory class which is discussed in next section.

### BeanCreatorFactory 
This is the factory class which will give creator implementation based on Bean scope

### SimpleApplicationContext

This class is an implementation of `BeanFactory` which implements `getBean` method for getting the bean based on the bean type. There can be multiple implementation for Bean factories based on the requirement.
This class does not know who will be the creator of the bean and and where are the beans stored and how they are rendered. This method only handles field injection for now and this logic can be moved to Injectors in next phase of development.

### Circular dependency detection 
Circular dependency detection is done using the Hash collection by storing the class types which visiting all the dependencies and it keeps track of the visited nodes and throw exception if there are any circular dependencies while creation of beans.

## What could be done better
    * MultiThreading support
    * Create different types of injectors like constructor and setter injector
    * Getting bean by name and other features
    * Include more test cases
    * Add java docs for code

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a system.

### Prerequisites

What things you need to install the software and how to install them

[Java 13](https://docs.oracle.com/en/java/javase/13/install/overview-jdk-installation.html#GUID-8677A77F-231A-40F7-98B9-1FD0B48C346A)

[Maven 4.0](http://maven.apache.org/install.html)

### Installing

First run the command maven clean

```
mvn clean
```

Then Compile the project

```
mvn compile
```

Then package and install the project using

```
mvn package install
```

## Running the tests

Run all the unit test classes.

```
mvn test
```



### Who do I talk to? ###

* Repo owner or admin

    <p> Naveen Kumar B  <br> naveen.win1@gmail.com<p>
    
* Other community or team contact